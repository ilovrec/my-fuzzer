# My Fuzzer

This is a fuzzer developed as a bachelor thesis for the Faculty of Electrical Engineering and Computing.

## Introduction
Unlike most fuzzers, My fuzzer uses two techniques instead of one while generating
input which makes it more successful. By using both mutations and generating from grammar
the fuzzer contains good sides of both  techniques. My fuzzer can produce valid inputs
from grammar and can also do fast mutations on inputs which results in good speed
and also good code coverage.


## Requirements
A file with grammar is required to start the fuzzer and the format is shown in [url_gramatika.txt](https://gitlab.com/ilovrec/my-fuzzer/blob/master/Docs/url_gramatika.txt).

The tested program should be compiled using gcov instrumentation using command below.

```bash
gcc -fprofile-arcs -ftest-coverage -o program program.c
```

## Running the fuzzer
To run the fuzzer the user must set a few parameters on which the fuzzing depends.
The parameters depend on the program which is being tested so it is up to the user
to set the right parameters for optimal testing.


