import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Mutations {

	 public static List<String> generateInputsMutational(int generated_inputs, Map<String, List<String>> grammar, List<String> interesting_inputs, List<String> current_inputs, int mutation_loop){
		   List<String> inputs_to_mutate;
		   List<String> terminals= Grammar.getTerminals(grammar);
		   List<String> mutated_inputs= new LinkedList<>();
		   
		   int max_mutations=10 + 10*mutation_loop;    //svaki loop povecava maksimalni broj mutacija
		   
		   if(interesting_inputs.isEmpty())
			   inputs_to_mutate=current_inputs;
		   else
			   inputs_to_mutate= interesting_inputs;
		   
		   for(int i=0; i < generated_inputs; i++) {
			   String to_mutate= Fuzzer.getRandomElement(inputs_to_mutate);
			   String mutated= Mutations.mutateMultiple(max_mutations, to_mutate, terminals);
			   mutated_inputs.add(mutated);
		   }
		   return mutated_inputs;
		   
		   
	   }
	
	
	
	
	public static String mutateInput(int method, String to_mutate, List<String> using_exp) {
		String mutated;
		switch(method) {
			case 1: { mutated= Mutations.insertMutation(to_mutate, using_exp); break; }
			case 2: { mutated= Mutations.deleteMutation(to_mutate); break; }
			case 3: { mutated= Mutations.filpMutation(to_mutate); break; }
			case 4: { mutated= Mutations.replaceMutation(to_mutate, using_exp); break; }
			default:  mutated= Mutations.insertMutation(to_mutate, using_exp);
		}
		return mutated;
	}
	
	public static String mutateMultiple(int max_mutations, String to_mutate, List<String> using_exp ) {
		String new_result= to_mutate;
		String old_result= to_mutate;
		int mutations=(int) (1+Math.random()*(max_mutations));
		int method=1;
		for(int i=0; i<mutations; i++) {
			method=(int) (1+Math.random()*4);
			new_result= mutateInput(method, old_result, using_exp);
			old_result=new_result;
		}
		//System.out.println("To_mutate: " + to_mutate +"      old res: "+old_result + "       new res: "+new_result);
		return new_result;
		
	}
	
	public static String insertMutation(String to_mutate, List<String> using_exp) {
		String result;
		int r=(int) (0+Math.random()*(to_mutate.length()+1));
		String to_insert= Fuzzer.getRandomElement(using_exp);
		result= to_mutate.substring(0, r) + to_insert + to_mutate.substring(r);
		return result;
	}
	
	public static String deleteMutation(String to_mutate) {
		String result;
		int r=(int) (0+Math.random()*(to_mutate.length()+1));
		if(r<to_mutate.length())
			result= to_mutate.substring(0, r) + to_mutate.substring(r+1);
		else 
			result= to_mutate.substring(0, to_mutate.length()-1);
		
		
		//testni
		//result= to_mutate.substring(0, to_mutate.length()-1);
		//result= to_mutate.substring(0, 4) + to_mutate.substring(4+1);
		return result;
	}
	
	public static String filpMutation(String to_mutate) {
		String result;
		int r=(int) (0+Math.random()*(to_mutate.length()-1));
		
		result = Mutations.swap(to_mutate, r);
		
		//testni
		//result = Mutations.swap(to_mutate, 2, 3);
		//result = Mutations.swap(to_mutate, to_mutate.length()-2, to_mutate.length()-1);
		return result;
	}
	
	public static String replaceMutation(String to_mutate, List<String> using_exp) {
		String result;
		int r=(int) (0+Math.random()*(to_mutate.length()+1));
		String to_insert= Fuzzer.getRandomElement(using_exp);
		if(r<to_mutate.length())
			result= to_mutate.substring(0, r) + to_insert + to_mutate.substring(r+1);
		else
			result= to_mutate.substring(0, to_mutate.length()-1) + to_insert;
		//testni
		//result= to_mutate.substring(0, 4) + to_insert + to_mutate.substring(4+1);
		//result= to_mutate.substring(0, to_mutate.length()-1) + to_insert ;
		
		return result;
	}
	
	
	public static String swap(String s, int i0) {
		if (s.length()<2) return s;
		if(i0+1<s.length())
		  return s.substring(0, i0) + s.charAt(i0+1) +s.charAt(i0) + s.substring(i0+2);
		else 
			return s.substring(0, i0) + s.charAt(i0+1) +s.charAt(i0);
		}
	
	public static void Test(String to_mutate, List<String> using_exp) {
		for (int i=0; i<10; i++) {
			String result= Mutations.mutateMultiple(10, to_mutate, using_exp);
			System.out.println("Ulaz: "+ to_mutate +" \nIzlaz: "+result +"\n\n");
		}
	}
}
