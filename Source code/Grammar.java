import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Grammar {
	
	public static Map<String, List<String>>  getGrammarFromFile( String filename){
		Map<String, List<String>> gramm= new LinkedHashMap<>();
		try{
			
		FileInputStream fs= new FileInputStream(filename);
		BufferedReader br = new BufferedReader(new InputStreamReader(fs));
		String line= br.readLine();
		
		while(line !=null){
			String[] parts= line.split("::=");    // parts[0] je nezavrsni znak, parts[1] je lista transformacija
			String[] transform = parts[1].split(Pattern.quote("|"));
			List<String> transformations = new LinkedList<>();
			for(String t: transform)
				transformations.add(t.trim());
			gramm.put(parts[0].trim(), transformations);
			line=br.readLine();
		}
		br.close();
		}catch(Exception e) {
			System.out.println("Neuspjelo Ã„ï¿½itanje gramatike");
			System.out.println(e);
		}
	
		return gramm;
	 }
	
	
	
	
	
	public static List<String> generateInputsGrammar(int generated_inputs, Map<String, List<String>> grammar, int max_nonterm, int  max_expansion_trials) throws Exception{
		 int expansion_trails=0;
		 List<String> current_inputs= new LinkedList<>();
		 String start =grammar.keySet().iterator().next();
		 String term;
		 
		
		for(int i=0; i< generated_inputs; i++) {
			 term=start;
			 
		 while ( Grammar.getNonterminal(term).size() >0 ){
			
			 String simbol_to_expand= Fuzzer.getRandomElement( Grammar.getNonterminal(term));
			 List<String> expansions= grammar.get(simbol_to_expand);
			 String using_expansion= Fuzzer.getRandomElement(expansions);
			 String new_term = term.replaceFirst(simbol_to_expand, using_expansion);
			 
			 if(Grammar.getNonterminal(new_term).size() < max_nonterm) {
				 term=new_term;
				 expansion_trails=0;
			 }
			 else {
				 ++expansion_trails;
				 if(expansion_trails >= max_expansion_trials)
					 throw new Exception("PrekoraÄ�en broj koraka u transformaciji");
			 }
		 }
		 
		  current_inputs.add(term);
		}
		
		 //System.out.println("\nGenerirani input:   "+current_inputs.toString());
		 return current_inputs;
		 
	 }
	
	
	   public static List<String> getNonterminal( String exp) {
			 List<String> lista= new LinkedList<>();
			 Pattern p = Pattern.compile("\\<(.*?)\\>");
			Matcher m = p.matcher(exp);
			while(m.find())
				lista.add(m.group());
			/*or( String e: elementi){
				 if (e.startsWith("<") && e.endsWith(">"))
					 lista.add(e);
			 }   */
			 return lista;
		 } 
	   
	   
	   public static List<String> getTerminals( Map<String, List<String>> grammar){
		   List<String> terminals= new LinkedList<>();
		   String terminal;
		   for (Map.Entry<String, List<String>> entry : grammar.entrySet()) {
			   List<String> extensions= entry.getValue();
			   for(String exp : extensions) {
				   Pattern p = Pattern.compile("\\>(.*?)\\<");
					Matcher m = p.matcher(exp);
					while(m.find()) {
						terminal=m.group().substring(1, m.group().length()-1);
						if(!terminal.equals(""))
							terminals.add(terminal.replaceAll("\\s", ""));
					 		//terminals. add(m.group());      ako  treba zadrÅ¾at razmak
					}
					if(Grammar.getNonterminal(exp).isEmpty())
						terminals.add(exp.replaceAll("\\s", ""));
			   }
			   
			}
		   return terminals;
	   }
	   

	   
	    

}
