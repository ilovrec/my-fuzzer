import java.io.*;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.nio.file.Paths;
import java.nio.file.Files;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.lang.StringBuilder;
import java.io.FileWriter; 
import java.util.Scanner;


public class Fuzzer {
	private int fuzz_tech;     // 0-mutational,  1-gramatika
	private Map<String, List<String>> grammar;
	private int max_nonterm;
	private int  max_expansion_trials;
	private List<String> interesting_inputs;
	private List<String> current_inputs;
	private Set<Integer> executed_lines;
	private int gramm_loop;
	private int num_loop;
	private int mutation_loop;
	private int last_interesting;
	private List<Double> all_coverages;
	private List<Double> max_mutational;
	private double avg_cov;
	private static PrintWriter crashes_printer;


	public Fuzzer(String filename) {
		fuzz_tech=1;
		grammar= Grammar.getGrammarFromFile(filename);
		max_nonterm=10;
		max_expansion_trials=100;
		interesting_inputs= new LinkedList<>();
		current_inputs= new LinkedList<>();
		executed_lines= new TreeSet<>();
		num_loop=0;
		last_interesting=0;
		all_coverages=new LinkedList<>();
		gramm_loop=0;
		max_mutational= new LinkedList<>();
		mutation_loop=0;
		avg_cov=0;
	try{
			File crashes = new File("crashes.txt");
			this.crashes_printer = new PrintWriter(crashes);
			crashes_printer.append(" Ulazi koji su uzrokovali pad programa: \n\n\n");
			
		}catch (Exception e){
    	   		System.out.println(e.getCause());
		}
		
		
	
		
	}
	
	public Fuzzer(String filename, int max_n, int max_et) {
		fuzz_tech=0;
		grammar= Grammar.getGrammarFromFile(filename);
		max_nonterm=max_n;
		max_expansion_trials=max_et;
	}
	

	public void setFuzz_tech(int i) { 
		if(i==0 || i==1)
			this.fuzz_tech=i;
		else System.out.println("Krivi argument za tehniku fuzziranja");
	}
	
	

//kompajliranje c programa: gcc -fprofile-arcs -ftest-coverage -o url_parser url_parser.c
	 
   public static void main(String[] args) {
	   Fuzzer fuzzer= new Fuzzer("url_gramatika");
	   Map<String, List<String>> gramm= fuzzer.grammar;
	   //System.out.println("Zadana gramtika : \n" + gramm.toString());
	System.out.println("Gramatika uspješno spremljena u mapu");

		Runtime.getRuntime().addShutdownHook(new Thread() {
        public void run() {
            try {
                Thread.sleep(200);
                System.out.println("Fuzzer se gasi...");
		crashes_printer.close();
                //some cleaning up code...

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    });
	   
	   List<Double> all_coverages= new LinkedList<>();
	   Double current_coverage; 
	
	   

		Scanner reader = new Scanner(System.in);  // Reading from System.in
		System.out.println("Naziv programa: ");
		String program_name = reader.next();
		System.out.println("Broj ulaza po ciklusu: ");
		int generated_inputs = reader.nextInt();
		System.out.println("Broj ciklusa koji želite izvesti (moguće prekinuti ranije uz CTRL+C): ");
		int cicles = reader.nextInt();
		System.out.println("Broj potrebnih zanimljivih ulaza za prijelaz na mutaciju: ");
		int needed_interesting = reader.nextInt();
	   	System.out.println("Tolerancija za pokrivenost pri mutaciji: ");
		double tolerance= reader.nextDouble();
		System.out.println("Granica pada pokrivenosti pri mutaciji: ");
		double perc= reader.nextDouble();
		System.out.println("Tolerancija je: "+tolerance);

		int crashed=0;

		

	   while(fuzzer.num_loop<cicles) {
		   crashed=0;
		   
		   fuzzer.updateLoopNumber();
		   boolean to_grammar=fuzzer.decideFuzzingTechnique(tolerance, perc, needed_interesting);
		   if(to_grammar)
			fuzzer.resetCoverageInfo();
		   
		   if(fuzzer.fuzz_tech==0)
		   	System.out.println("\n\nZapočeo novi ciklus s tehnikom: Mutacija");
		   else
			System.out.println("\n\nZapočeo novi ciklus s tehnikom: Generacija iz gramatike");
		   try {
			   
			   fuzzer.createInput(generated_inputs);
		   } catch (Exception e) {
			   e.printStackTrace();
		   } 
		//System.out.println("Broj inputa: "+fuzzer.current_inputs.size());
		  
		   for(String input : fuzzer.current_inputs) {
	
			current_coverage=Fuzzer.runProgram(input,program_name);
			
			if(current_coverage > 0.0)
				fuzzer.all_coverages.add(current_coverage);
			else 
				crashed++;
			
				
			fuzzer.isInputInteresting(input,current_coverage, program_name);
			
				
			//System.out.println("\n\n\n\n\n\n");
	
	  }
		
		//System.out.println("Sve pokrivenosti:" +fuzzer.all_coverages);
		   if(fuzzer.fuzz_tech ==0 )
			fuzzer.prepareForNextMutational();
		if(crashed>0) 
			System.out.println("Broj rušenja programa u ciklusu:"+ crashed+", ulazi koji su srušili program su pohranjeni u crashes.txt");
		System.out.println("Broj zanimljivih ulaza: "+fuzzer.interesting_inputs.size());
		   
		   //fuzzer.max_coverage_loop.add(Collections.max(all_coverages));
	
	   }

	}

public void updateLoopNumber(){
	this.num_loop++;
	if(fuzz_tech==0)
		this.mutation_loop++;
	if(fuzz_tech==1)
		this.gramm_loop++;
}

public void prepareForNextMutational(){
	Double max_cov= Collections.max(this.all_coverages);
	this.max_mutational.add(max_cov);
	//System.out.println("Maksimalne pokrivenosti u mutacijskim ciklusima: " +max_mutational.toString());
	this.all_coverages.clear();
	
}

public void resetCoverageInfo(){
	this.interesting_inputs.clear();
	this.all_coverages.clear();
	this.executed_lines.clear();
}
   
   

public void isInputInteresting(String input, Double coverage, String program_name) {
	 Set<Integer> exec_lines= Coverage.getCoveredLines(program_name);
	   //System.out.println("Broj prodenih linije : \n" +exec_lines.size());


		
	
	   if(!this.executed_lines.containsAll(exec_lines)) {
		   System.out.println("Nađen zanimljiv ulaz:" + input);
		   interesting_inputs.add(input);
		   executed_lines.addAll(exec_lines);

		//bitno je pratit zadnji zanimljivi input samo za mutacije, da se prebaci ak nema zanimljivih
		if(this.fuzz_tech==0)
		   this.last_interesting=this.num_loop;
	   }
	 
	
}


   public static Double runProgram(String input, String program_name){
      //System.out.println("Pokrecem program, input "+input);
       try{
    		  

	
	String s;
	File file = new File(program_name+".gcda");
	if (file.exists()) 
    		file.delete();
	
	
	//TimeUnit.SECONDS.sleep(5);
			
	
	Process p2 = Runtime.getRuntime().exec("./"+program_name+ " " +input);
	//Process p2 = Runtime.getRuntime().exec("./"+program_name+ " <ulaz");
	//System.out.println("Pozivam komandu: ./"+program_name+ " "+input);
	BufferedReader reader =
               new BufferedReader(new InputStreamReader(p2.getInputStream()));
	String line; boolean empty=true;
	//System.out.println("\n\n\n\n\n Output je: ");
            while ((line = reader.readLine()) != null) {
                //System.out.println(line);
		empty=false;
            }


	p2.waitFor();
	p2.destroy();
        Process p3 = Runtime.getRuntime().exec("gcov -c -b "+program_name);
	
        BufferedReader br = new BufferedReader(new InputStreamReader(p3.getInputStream())); 
	Double coverage= Coverage.getCoverageWithReader(br); 
	
            p3.waitFor();
            //System.out.println ("exit: " + p3.exitValue());
            p3.destroy(); 
		//Fuzzer.getOutput("izlaz"); 

	if(empty==true) {
		//System.out.println("Program se srusio, ulaz se sprema ");
		Fuzzer.crashes_printer.append("	  "+input+"\n\n");
		
   		
   		
		
				
	}

    	return coverage;



       }catch (Exception e){
    	   System.out.println(e.getCause());
         System.out.println("Neuspješno pokretanje programa");
         
       }
	return null;
    }

public static void getOutput(String filename){
try{
	FileInputStream fs= new FileInputStream(filename);
		BufferedReader br = new BufferedReader(new InputStreamReader(fs));
		System.out.println("\nOutput je: \n");
		String line= br.readLine();
		while(line !=null){
			System.out.println(line);
			line=br.readLine();
		}
	}catch(Exception e) {
			System.out.println("Neuspjelo citanje outputa");
			System.out.println(e);
		}
}
   

   public boolean decideFuzzingTechnique(double tolerance, double perc, int needed_interesting) {
	  
	   if(this.interesting_inputs.size() >= needed_interesting && this.fuzz_tech==1) {
		   this.setFuzz_tech(0);
		   reset();
		   return false;
	   }else if(this.fuzz_tech==0){
		if(!this.checkMutationalCoverage(tolerance, perc)){
			this.setFuzz_tech(1);
			reset();
		  	return true;
		}
		return false;
	   }
	   return false;
   }

   //briše stare podatke tako da nova iteracija s promjenjenom tehnikom radi dobro
   public void reset(){
	
	if(this.fuzz_tech==0){    //promjenjeno iz grammar u mutational
		max_mutational.clear();
		mutation_loop=0;
		this.last_interesting=this.num_loop;
		//all_coverages.clear();
		this.avg_cov= this.all_coverages.stream().mapToDouble(val -> val).average().orElse(0.0);
		System.out.println("Nađeno dovoljno zanimljivih inputa, kreće na mutaciju");
		//System.out.println("Zanimljvi ulazi su: "+interesting_inputs+"\n\n");
		
	}
	
	if(this.fuzz_tech==1){    //promjenjeno iz mutational u grammar
		interesting_inputs.clear();
		all_coverages.clear();
		executed_lines.clear();
		max_nonterm++;			 //za svaku generaciju iz gramatike su sve kompleksniji inputi generirani
		max_expansion_trials+=5;
		System.out.println("Prelazi se na generaciju iz gramatike");
	}
   }


  public boolean checkMutationalCoverage(double tolerance, double perc){
	System.out.println("Zadnja maksimalna pokrivenost:" + this.max_mutational.get(max_mutational.size()-1) +"\nProsječna pokrivenost-tolerancija:" +(this.avg_cov-tolerance));
	if(sinceInterestingInput()<=3)  //pusti ga da jos mutira s novim zanimljivim
		return true;
	if(this.max_mutational.get(max_mutational.size()-1) < this.avg_cov-tolerance){
		System.out.println("Mutacija neuspješna, razlog: Loša pokrivenost programa");
		return false;
	}
	if(sinceInterestingInput()>10){
		System.out.println("Mutacija neuspješna, razlog: Nema novih zanimljivih ulaza");
		return false;
	}
	if(this.constantDeclineMutational(4, perc)){
		System.out.println("Mutacija neuspješna, razlog: Pokrivenost konstantno opada");
		return false;
	}
	return true;

}



  //ako zadnjih num_elem coverage su svaki manji za 5% od prijasnjeg se mijenja nazad na grammar
  public boolean constantDeclineMutational(int num_elem, double perc){

	//nema smisla usporedivat manje od zadnog num_elem
	if(num_elem>this.max_mutational.size())
		return false;
		
	boolean to_return=true;
	for(int i=this.max_mutational.size()-num_elem; i<this.max_mutational.size()-1; i++){
		Double first_cov= this.max_mutational.get(i);
		Double second_cov= this.max_mutational.get(i+1);
		if(first_cov-second_cov < perc)
			return false;
	}
	return true;

}
   
   public boolean changeTechnique() {
	   if(this.fuzz_tech==0){
		   this.setFuzz_tech(1);
		   return true;
		}
	   else if(this.fuzz_tech==1) {
		   this.setFuzz_tech(0);
		   return false;
	   }
	   this.last_interesting= this.num_loop;
		return false;
   }
   
   public int sinceInterestingInput() {
	   return this.num_loop-this.last_interesting;
   }
   
   

   public void createInput(int generated_inputs){
	 if(fuzz_tech==0)
		 this.current_inputs= Mutations.generateInputsMutational(generated_inputs, grammar, interesting_inputs, current_inputs, this.mutation_loop);
	
	if(fuzz_tech==1){
		try {
			this.current_inputs= Grammar.generateInputsGrammar(generated_inputs, grammar,max_nonterm, max_expansion_trials);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
   }
   
   
  
   
   
   public static String getRandomElement(List<String> list) 
   { 
       Random rand = new Random(); 
       return list.get(rand.nextInt(list.size())); 
   }
    
    
   
    
    
    
 
	 
}

   

