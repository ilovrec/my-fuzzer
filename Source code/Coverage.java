import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;
import java.util.*; 


public class Coverage {
	
	public static Set<Integer> getCoveredLines(String program_name) {
		Set<Integer> covered= new TreeSet<>();
		FileInputStream fs;
		try {
			fs = new FileInputStream(program_name+".c.gcov");
			BufferedReader br = new BufferedReader(new InputStreamReader(fs));
			String line=br.readLine();
	
    		while(line!=null) {
    			String[] split_line= line.split(":");
			if(split_line.length>=2){
    				if(Coverage.isNumeric(split_line[0]) && Coverage.isNumeric(split_line[1]) ) {
    					covered.add(Integer.parseInt(split_line[1].trim()));
    				}
			}
    			//System.out.println(line);
    			line=br.readLine();
    		}
    		
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Ne postoji gcov file");
			e.printStackTrace();
		}
		return covered;
		
	}

	public static Set<Integer> getCoveredLinesLCOV(){
		Set<Integer> covered= new TreeSet<>();
		 System.out.println("Pokrenut citanje lcov");
		FileInputStream fs;
		try {
			fs = new FileInputStream("coverage.main.info");
			BufferedReader br = new BufferedReader(new InputStreamReader(fs));
			String line=br.readLine();
	
    		while(line!=null) {
			/*format linije s lcov:   DA:133,0      -pazi ima i 0 izvedeno
						  DA:136,10 */
			
    			String[] split_line= line.split(":");
			if(split_line.length>=2){
				String[] num_line= split_line[1].split(",");
	
    				if( split_line[0].equals("DA")) {
					
					if(Integer.parseInt(num_line[1].trim())>0){
    						covered.add(Integer.parseInt(num_line[0].trim()));
					
					}
    				}
			}
    			
    			line=br.readLine();
			
    		}
    		
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Problem s citanjem lcov");
			e.printStackTrace();
		}
		
		//System.out.println("Prodene linije su: "+ covered.toString());
		return covered;
	}

	public static Double getCoverageWithReader(BufferedReader br){
		try{
		for(int i = 0; i <1; ++i)
    		  br.readLine();
		
    		
    		String all_coverage= br.readLine();
		//System.out.println("Linija s pokrivenosti"+all_coverage);
    		all_coverage=all_coverage.substring(15, 20);
		if(all_coverage.contains("%")) 
			all_coverage=all_coverage.substring(0, 4);
		//System.out.println("Postotak pokrivenosti iz readera: "+all_coverage );
    		Double num_all_cov= Double.parseDouble(all_coverage);
		/*String line=br.readLine();
		while(line!=null) {
    			System.out.println(line);
    			line=br.readLine();
    		}  */
		//return  Double.parseDouble("2");
    		return num_all_cov;
		}catch(Exception e)
    		{System.out.println("Neuspjelo citanje pokrivenosti iz bufferedReadera");
    		 System.out.println(e);}
		return null;
	}
	
	public static Double getCoverage(){
    	try{
    		FileInputStream fs= new FileInputStream("coverage");
    		BufferedReader br = new BufferedReader(new InputStreamReader(fs));
    		
    		for(int i = 0; i <1; ++i)
    		  br.readLine();
		
    		
    		String all_coverage= br.readLine();
		//System.out.println("Linija s pokrivenosti"+all_coverage);
    		all_coverage=all_coverage.substring(15, 20);
		if(all_coverage.contains("%")) 
			all_coverage=all_coverage.substring(0, 4);
		System.out.println("Postotak pokrivenosti: "+all_coverage);
    		Double num_all_cov= Double.parseDouble(all_coverage);
		String line=br.readLine();
		while(line!=null) {
    			//System.out.println(line);
    			line=br.readLine();
    		} 
		//return  Double.parseDouble("2");
    		return num_all_cov;
    		
    		
    	}catch(FileNotFoundException e){
		System.out.println("Ne postoji file coverage");
    		
    	}catch(Exception e)
    		{System.out.println("Neuspjelo citanje pokrivenosti");
    		 System.out.println(e);}
    	return null;
    	}
	
	public static boolean isNumeric(String str) { 
		  try {  
		    Double.parseDouble(str);  
		    return true;
		  } catch(NumberFormatException e){  
		    return false;  
		  }  
		}

}
